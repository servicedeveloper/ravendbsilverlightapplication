﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Windows.Interactivity;


namespace RavenDBSilverlightApplication
{
    public class TextChangedBehavior : Behavior<TextBox>
    {

        public bool IsAutoFocus { get; set; }


        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.TextChanged += (sender, e) =>
            {
                if (FocusManager.GetFocusedElement() == AssociatedObject)
                    if (ModelState != DataModelState.editing && ModelState != DataModelState.inserting)
                        ModelState = DataModelState.editing;
                if (ModelState == DataModelState.editing || ModelState == DataModelState.inserting)
                {
                    var expression = AssociatedObject.GetBindingExpression(TextBox.TextProperty);
                    if (expression != null)
                        expression.UpdateSource();
                }
            };
        }


        public DataModelState ModelState
        {
            get { return (DataModelState)GetValue(ModelStateProperty); }
            set { SetValue(ModelStateProperty, value); }
        }

        public static readonly DependencyProperty ModelStateProperty = DependencyProperty.Register("ModelState", typeof(DataModelState), typeof(TextChangedBehavior), new PropertyMetadata((d, e) => {
            var behavior = d as TextChangedBehavior;
            if (behavior.IsAutoFocus && ((DataModelState)e.NewValue == DataModelState.inserting)) behavior.AssociatedObject.Focus();
        }));

    }
}
