﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Windows.Interactivity;
using System.Windows.Data;

using System.Linq;
using System.Collections;

namespace RavenDBSilverlightApplication
{

    public class TreeViewBehavior : Behavior<TreeView>
    {

        protected override void OnAttached()
        {
            base.OnAttached();
            AssociatedObject.SelectedItemChanged += (sender, e) =>
            {
                this.SelectedItem = e.NewValue;
            };

            AssociatedObject.LayoutUpdated += (sender, e) =>
            {
                AssociatedObject.ExpandAll();
            };
        }


        public object SelectedItem
        {
            get { return GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(TreeViewBehavior), new PropertyMetadata((d, e) =>
            {
                var behavior = d as TreeViewBehavior;
                if (e.NewValue == null) {
                    var selectedTVI = behavior.AssociatedObject.GetSelectedContainer() as TreeViewItem;
                    if (selectedTVI != null) selectedTVI.IsSelected = false;
                }
                else
                    behavior.AssociatedObject.SelectItem(e.NewValue);
            }));

    }

}
