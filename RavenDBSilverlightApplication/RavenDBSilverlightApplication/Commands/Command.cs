﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.ComponentModel;


namespace RavenDBSilverlightApplication
{
    public class Command : DependencyObject, ICommand
    {

        public event EventHandler CanExecuteChanged;
        public event DoWorkEventHandler Executed;


        public bool CanExecute(object parameter)
        {
            return this.IsEnabled;
        }


        public void Execute(object parameter = null)
        {
            if (Executed != null)
                Executed.Invoke(this, new DoWorkEventArgs(parameter));
        }


        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(Command), new PropertyMetadata(true, (d, e) =>
        {
            var command = d as Command;
            if (command.CanExecuteChanged != null)
                command.CanExecuteChanged.Invoke(command, new EventArgs());
        }));


    }
}
