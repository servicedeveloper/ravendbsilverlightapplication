﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;


namespace RavenDBSilverlightApplication
{

    public class IntentionsModel : DataModel
    {

        Intention backupObject;


        public IntentionsModel()
        {
            RefreshCommand.Executed += (sender, e) => { Refresh(); };

            AddCommand.Executed += (sender, e) =>
            {
                var newItem = new Intention() { Id = DateTime.Now.Ticks };
                Intentions.Add(newItem);
                SelectedIntention = newItem;
                ModelState = DataModelState.inserting;
            };

            DeleteCommand.Executed += (sender, e) =>
            {
                if (this.SelectedIntention != null)
                    this.DBService.Delete<Intention>(this.SelectedIntention, (exception) =>
                    {
                        if (exception == null)
                        {
                            this.Intentions.Remove(this.SelectedIntention);
                            this.SelectedIntention = null;
                        }
                    });
            };

            CancelCommand.Executed += (sender, e) =>
            {
                if (ModelState == DataModelState.inserting)
                {
                    Intentions.RemoveAt(0);
                    ModelState = DataModelState.none;
                }
                if (ModelState == DataModelState.editing)
                {
                    if (this.SelectedIntention != null && this.backupObject != null)
                    {
                        var canceledObject = this.SelectedIntention;
                        this.Intentions.Insert(this.Intentions.IndexOf(canceledObject), this.backupObject);
                        this.Intentions.Remove(canceledObject);
                        this.SelectedIntention = this.backupObject;
                        this.backupObject = null;
                    }
                }
            };

            SubmitCommand.Executed += (saender, e) =>
            {
                if (this.SelectedIntention == null) return;
                this.DBService.Update<Intention>(this.SelectedIntention, (exception) =>
                {
                    if (exception == null)
                    {
                        this.backupObject = null;
                        ModelState = DataModelState.selected;
                    }
                });
            };

            Refresh();
        }

        void Refresh()
        {
            this.ModelState = DataModelState.loading;
            this.DBService.Get<Intention>((result) =>
            {
                this.Intentions = new ObservableCollection<Intention>(result);
                this.ModelState = DataModelState.none;
            });
        }

        protected override void StateChanged(DataModelState oldValue, DataModelState newValue)
        {
            if (newValue == DataModelState.editing && SelectedIntention != null)
                backupObject = SelectedIntention.Clone() as Intention;
        }


        public ObservableCollection<Intention> Intentions
        {
            get { return (ObservableCollection<Intention>)GetValue(IntentionsProperty); }
            set { SetValue(IntentionsProperty, value); }
        }

        public static readonly DependencyProperty IntentionsProperty =
            DependencyProperty.Register("Intentions", typeof(ObservableCollection<Intention>), typeof(IntentionsModel), null);



        public Intention SelectedIntention
        {
            get { return (Intention)GetValue(SelectedIntentionProperty); }
            set { SetValue(SelectedIntentionProperty, value); }
        }

        public static readonly DependencyProperty SelectedIntentionProperty =
            DependencyProperty.Register("SelectedIntention", typeof(Intention), typeof(IntentionsModel), new PropertyMetadata((d, e) =>
        {
            var model = d as IntentionsModel;
            model.ModelState = e.NewValue == null ? DataModelState.none : DataModelState.selected;
        }));

    }

}
