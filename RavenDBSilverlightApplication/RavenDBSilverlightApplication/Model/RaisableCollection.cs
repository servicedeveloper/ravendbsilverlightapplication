﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Collections.Specialized;


namespace RavenDBSilverlightApplication
{
    public class RaisableCollection<T> : ObservableCollection<T>
    {

        public RaisableCollection(IEnumerable<T> collection)
            : base(collection)
        {
        }



        public void RaiseCollectionChanged()
        {
            OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }

    }
}
