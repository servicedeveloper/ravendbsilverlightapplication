﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Raven.Client.Document;
using System.ComponentModel;
using System.Windows.Data;
using System.Collections.Generic;


namespace RavenDBSilverlightApplication
{

    public abstract class DataModel : DependencyObject
    {

        const string serverAddress = "http://servicedeveloper.org/Examples/RavenDBServer";
        //const string serverAddress = "http://localhost/RavenDBServer";

        public Command RefreshCommand { get; protected set; }
        public Command AddCommand { get; protected set; }
        public Command DeleteCommand { get; protected set; }
        public Command CancelCommand { get; protected set; }
        public Command SubmitCommand { get; protected set; }

        protected IDataModelService DBService = new RavenDBService();
        protected DocumentStore documentStore = new DocumentStore { Url = serverAddress };


        public DataModel()
        {
            RefreshCommand = new Command();
            BindingOperations.SetBinding(RefreshCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "!loading||editing||inserting"
            });

            AddCommand = new Command();
            BindingOperations.SetBinding(AddCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "!loading||editing||inserting"
            });

            DeleteCommand = new Command();
            BindingOperations.SetBinding(DeleteCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "selected"
            });

            CancelCommand = new Command();
            BindingOperations.SetBinding(CancelCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "editing||inserting"
            });

            SubmitCommand = new Command();
            BindingOperations.SetBinding(SubmitCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "editing||inserting"
            });

            documentStore.Initialize();
        }

        protected virtual void StateChanged(DataModelState oldValue, DataModelState newValue)
        {
        }

        public DataModelState ModelState
        {
            get { return (DataModelState)GetValue(ModelStateProperty); }
            set { SetValue(ModelStateProperty, value); }
        }

        public static readonly DependencyProperty ModelStateProperty =
            DependencyProperty.Register("ModelState", typeof(DataModelState), typeof(DataModel), new PropertyMetadata(DataModelState.none, (d, e) =>
            {
                var model = d as DataModel;
                model.StateChanged((DataModelState)e.OldValue, (DataModelState)e.NewValue);
            }));

    }



    public enum DataModelState
    {
        none,
        loading,
        selected,
        editing,
        inserting
    }



    public interface IDataModelService
    {

        void Get<T>(Action<IList<T>> callback) where T : CustomEntity;
        void Update<T>(T item, Action<Exception> callback) where T : CustomEntity;
        void Insert<T>(T item, Action<Exception> callback) where T : CustomEntity;
        void Delete<T>(T item, Action<Exception> callback) where T : CustomEntity;
    }

}
