﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections;
using System.Linq;


namespace RavenDBSilverlightApplication
{

    public class CheckItemCollection<T> : DependencyObjectCollection<CheckItem<T>>
    {

        public event DependencyPropertyChangedEventHandler CheckItemChanged;

        public event DependencyPropertyChangedEventHandler EnabledItemChanged;


        public CheckItem<T> this[T index]
        {
            get
            {
                return this.FirstOrDefault(item => item.Item.Equals(index));
            }
        }


        public void AddItem(T item)
        {
            var checkItem = new CheckItem<T>(item);
            checkItem.IsCheckedChanged += new DependencyPropertyChangedEventHandler(checkItem_IsCheckedChanged);
            checkItem.IsEnabledChanged += new DependencyPropertyChangedEventHandler(checkItem_IsEnabledChanged);
            this.Add(checkItem);
        }

        public void AddItem(CheckItem<T> checkItem)
        {
            checkItem.IsCheckedChanged += new DependencyPropertyChangedEventHandler(checkItem_IsCheckedChanged);
            checkItem.IsEnabledChanged += new DependencyPropertyChangedEventHandler(checkItem_IsEnabledChanged);
            this.Add(checkItem);
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items) this.AddItem(item);
        }

        public void AddRange(IEnumerable<CheckItem<T>> checkItems)
        {
            foreach (var checkItem in checkItems) this.AddItem(checkItem);
        }

        public void RemoveItem(T item)
        {
            var checkItem = this.FirstOrDefault(collectionItem => collectionItem.Item.Equals(item));
            if (checkItem == null) return;
            checkItem.IsCheckedChanged -= new DependencyPropertyChangedEventHandler(checkItem_IsCheckedChanged);
            checkItem.IsEnabledChanged -= new DependencyPropertyChangedEventHandler(checkItem_IsEnabledChanged);
            this.Remove(checkItem);
        }

        public void SetCheckedItems<Tp>(IEnumerable<Tp> items, Func<Tp, T, bool> predicate)
        {
            foreach (var ownItem in this)
                ownItem.IsChecked = items.Any(item => predicate(item, ownItem.Item));
        }

        public Tr[] GetCheckedItems<Tr>(Func<T, Tr> resultReducer)
        {
            return (from item in this where item.IsChecked select resultReducer(item.Item)).ToArray();
        }

        void checkItem_IsCheckedChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.CheckItemChanged != null) this.CheckItemChanged.Invoke(sender, e);
        }

        void checkItem_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.EnabledItemChanged != null) this.EnabledItemChanged(sender, e);
        }

    }



    public class CheckItem<T> : DependencyObject
    {

        public event DependencyPropertyChangedEventHandler IsCheckedChanged;

        public event DependencyPropertyChangedEventHandler IsEnabledChanged;


        public CheckItem(T item)
        {
            this.Item = item;
        }


        public T Item
        {
            get;
            private set;
        }


        public bool IsChecked
        {
            get { return (bool)GetValue(IsCheckedProperty); }
            set { SetValue(IsCheckedProperty, value); }
        }

        public static readonly DependencyProperty IsCheckedProperty = DependencyProperty.Register("IsChecked", typeof(bool), typeof(CheckItem<T>), new PropertyMetadata(IsCheckedPropertyChangedCallback));

        static void IsCheckedPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var checkItem = d as CheckItem<T>;
            if (checkItem.IsCheckedChanged != null) checkItem.IsCheckedChanged.Invoke(checkItem, e);
        }


        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        public static readonly DependencyProperty IsEnabledProperty = DependencyProperty.Register("IsEnabled", typeof(bool), typeof(CheckItem<T>), new PropertyMetadata(true, IsEnabledPropertyChangedCallback));

        static void IsEnabledPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var checkItem = d as CheckItem<T>;
            if (checkItem.IsEnabledChanged != null) checkItem.IsEnabledChanged.Invoke(checkItem, e);
        }

    }

}
