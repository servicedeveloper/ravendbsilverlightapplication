﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Raven.Client.Document;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Linq;


namespace RavenDBSilverlightApplication
{

    public class ControlModel : DependencyObject
    {

        const string templatesAddress = "/RavenDBSilverlightApplication;component/Templates/";


        public List<SidePanelItem> SidePanelItems { get; private set; }

        public ControlModel()
        {
            SidePanelItems = new List<SidePanelItem>() 
            { 
                new SidePanelItem() {ItemName="Намерения", ModelName= "IntentionsModel", TemplateName= "IntentionsTemplate"}, 
                new SidePanelItem(){ItemName="Категории [шаблон1]", ModelName="CategoriesModel", TemplateName="CategoriesTemplate1"},
                new SidePanelItem(){ItemName="Категории [шаблон2]", ModelName="CategoriesModel", TemplateName="CategoriesTemplate2"}
            };
        }


        public SidePanelItem SidePanelSelectedItem
        {
            get { return (SidePanelItem)GetValue(SidePanelSelectedItemProperty); }
            set { SetValue(SidePanelSelectedItemProperty, value); }
        }

        public static readonly DependencyProperty SidePanelSelectedItemProperty =
            DependencyProperty.Register("SidePanelSelectedItem", typeof(SidePanelItem), typeof(ControlModel), new PropertyMetadata((d, e) =>
            {
                var model = d as ControlModel;
                var item = e.NewValue as SidePanelItem;
                var dictionary = new ResourceDictionary();
                dictionary.Source = new Uri(templatesAddress + item.TemplateName + ".xaml", UriKind.Relative);
                model.MainPanelTemplate = dictionary[item.TemplateName] as DataTemplate;
                var modelType = Assembly.GetExecutingAssembly().GetTypes().FirstOrDefault(type => type.Name == item.ModelName);
                if (modelType == null)
                    model.MainPanelContent = null;
                else
                    model.MainPanelContent = modelType.Assembly.CreateInstance(modelType.FullName) as DataModel;
            }));


        public DataTemplate MainPanelTemplate
        {
            get { return (DataTemplate)GetValue(MainPanelTemplateProperty); }
            set { SetValue(MainPanelTemplateProperty, value); }
        }

        public static readonly DependencyProperty MainPanelTemplateProperty = DependencyProperty.Register("MainPanelTemplate", typeof(DataTemplate), typeof(ControlModel), null);


        public DataModel MainPanelContent
        {
            get { return (DataModel)GetValue(MainPanelContentProperty); }
            set { SetValue(MainPanelContentProperty, value); }
        }

        public static readonly DependencyProperty MainPanelContentProperty = DependencyProperty.Register("MainPanelContent", typeof(DataModel), typeof(ControlModel), null);
    }



    public class SidePanelItem
    {
        public string ItemName { get; set; }
        public string ModelName { get; set; }
        public string TemplateName { get; set; }
    }

}
