﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Windows.Data;


namespace RavenDBSilverlightApplication
{

    public class CategoriesModel : DataModel
    {

        Category backupObject;


        public Command AddSubCategoryCommand { get; private set; }


        public CategoriesModel()
        {
            RefreshCommand.Executed += (sender, e) => { Refresh(); };

            AddCommand.Executed += (sender, e) =>
            {
                Category parent = null;
                if (this.SelectedCategory != null)
                    parent = GetParentCategory(this.SelectedCategory);
                var newCategory = new Category() { Id = DateTime.Now.Ticks };
                if (parent == null) this.Categories.Add(newCategory);
                else
                {
                    newCategory.IntentionIds = parent.IntentionIds;
                    parent.AddSubCategory(newCategory);
                    this.Categories.RaiseCollectionChanged();
                }
                Dispatcher.BeginInvoke(() =>
                {
                    this.SelectedCategory = newCategory;
                    ModelState = DataModelState.inserting;
                });
            };

            AddSubCategoryCommand = new Command();
            BindingOperations.SetBinding(AddSubCategoryCommand, Command.IsEnabledProperty, new Binding("ModelState")
            {
                Source = this,
                Converter = new ModelStateToBooleanConverter(),
                ConverterParameter = "selected"
            });
            AddSubCategoryCommand.Executed += (sender, e) =>
            {
                if (this.SelectedCategory == null) return;
                var newCategory = new Category() { Id = DateTime.Now.Ticks, IntentionIds = this.SelectedCategory.IntentionIds };
                this.SelectedCategory.AddSubCategory(newCategory);
                Categories.RaiseCollectionChanged();
                Dispatcher.BeginInvoke(() =>
                {
                    this.SelectedCategory = newCategory;
                    ModelState = DataModelState.inserting;
                });
            };

            DeleteCommand.Executed += (sender, e) =>
            {
                if (this.SelectedCategory == null) return;
                var parent = GetParentCategory(this.SelectedCategory);
                if (parent == null)
                    this.DBService.Delete<Category>(this.SelectedCategory, (exeption) =>
                    {
                        if (exeption == null)
                        {
                            Categories.Remove(this.SelectedCategory);
                            this.SelectedCategory = null;
                        }
                    });
                else
                {
                    var rootCategory = (GetRootCategory(this.SelectedCategory));
                    parent.DeleteSubCategory(this.SelectedCategory);
                    this.DBService.Update<Category>(rootCategory, (exception) =>
                    {
                        if (exception == null)
                            this.Categories.RaiseCollectionChanged();
                    });
                }
            };

            CancelCommand.Executed += (sender, e) =>
            {
                var parent = this.GetParentCategory(this.SelectedCategory);
                if (ModelState == DataModelState.inserting)
                {
                    if (parent == null)
                        this.Categories.Remove(this.SelectedCategory);
                    else
                    {
                        parent.DeleteSubCategory(this.SelectedCategory);
                        this.Categories.RaiseCollectionChanged();
                    }
                    this.SelectedCategory = null;
                    ModelState = DataModelState.none;
                }
                if (ModelState == DataModelState.editing)
                {
                    if (this.SelectedCategory != null && this.backupObject != null)
                    {
                        var canceledObject = this.SelectedCategory;
                        if (parent == null)
                        {
                            this.Categories.Insert(this.Categories.IndexOf(canceledObject), this.backupObject);
                            this.Categories.Remove(canceledObject);
                        }
                        else
                            parent.ReplaceSubCategory(canceledObject, this.backupObject);
                        this.Categories.RaiseCollectionChanged();
                        Dispatcher.BeginInvoke(() =>
                        {
                            this.SelectedCategory = this.backupObject;
                            this.backupObject = null;
                        });
                    }
                }
            };

            SubmitCommand.Executed += (saender, e) =>
            {
                using (var session = documentStore.OpenAsyncSession("Adverts"))
                {
                    var category = GetRootCategory(this.SelectedCategory);
                    this.DBService.Update<Category>(category, (exeption) =>
                    {
                        this.backupObject = null;
                        ModelState = DataModelState.selected;
                    });
                }
            };

            Refresh();
        }

        void Refresh()
        {
            this.ModelState = DataModelState.loading;
            this.DBService.Get<Intention>((result) =>
            {
                var checkableIntentions = new CheckItemCollection<Intention>();
                checkableIntentions.AddRange(result);
                this.Intentions = checkableIntentions;
            });
            this.DBService.Get<Category>((result) =>
            {
                this.Categories = new RaisableCollection<Category>(result);
                Dispatcher.BeginInvoke(() => { this.ModelState = DataModelState.none; });
            });
        }

        Category GetRootCategory(Category subCategory)
        {
            foreach (var category in this.Categories)
                if (category == subCategory || subCategory.GetParent(category) != null) return category;
            return null;
        }

        Category GetParentCategory(Category subCategory)
        {
            foreach (var category in this.Categories)
            {
                if (subCategory == category) return null;
                var parent = subCategory.GetParent(category);
                if (parent != null) return parent;
            }
            return null;
        }

        protected override void StateChanged(DataModelState oldValue, DataModelState newValue)
        {
            if (newValue == DataModelState.editing && SelectedCategory != null)
                backupObject = SelectedCategory.Clone() as Category;
        }


        public RaisableCollection<Category> Categories
        {
            get { return (RaisableCollection<Category>)GetValue(CategoriesProperty); }
            set { SetValue(CategoriesProperty, value); }
        }

        public static readonly DependencyProperty CategoriesProperty =
            DependencyProperty.Register("Categories", typeof(RaisableCollection<Category>), typeof(CategoriesModel), null);


        public Category SelectedCategory
        {
            get { return (Category)GetValue(SelectedCategoryProperty); }
            set { SetValue(SelectedCategoryProperty, value); }
        }

        public static readonly DependencyProperty SelectedCategoryProperty =
            DependencyProperty.Register("SelectedCategory", typeof(Category), typeof(CategoriesModel), new PropertyMetadata((d, e) =>
            {
                var model = d as CategoriesModel;
                var category = e.NewValue as Category;
                if (category == null)
                    model.ModelState = DataModelState.none;
                else
                {
                    if (model.Intentions != null)
                        model.Intentions.SetCheckedItems<long>(category.IntentionIds ?? new long[] { }, (intentionId, item) => item.Id == intentionId);

                    model.ModelState = DataModelState.selected;
                }
            }));


        public CheckItemCollection<Intention> Intentions
        {
            get { return (CheckItemCollection<Intention>)GetValue(IntentionsProperty); }
            set { SetValue(IntentionsProperty, value); }
        }

        public static readonly DependencyProperty IntentionsProperty =
            DependencyProperty.Register("Intentions", typeof(CheckItemCollection<Intention>), typeof(CategoriesModel), new PropertyMetadata((d, e) =>
            {
                var model = d as CategoriesModel;
                var oldIntentions = e.OldValue as CheckItemCollection<Intention>;
                if (oldIntentions != null) oldIntentions.CheckItemChanged -= model.Intentions_CheckItemChanged;
                var newIntentions = e.NewValue as CheckItemCollection<Intention>;
                if (newIntentions != null) newIntentions.CheckItemChanged += model.Intentions_CheckItemChanged;
            }));

        void Intentions_CheckItemChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (this.SelectedCategory == null || this.Intentions == null) return;
            var checkedItems = this.Intentions.GetCheckedItems<long>((item) => item.Id);
            this.SelectedCategory.IntentionIds = checkedItems.Length == 0 ? null : checkedItems;
            if (this.ModelState != DataModelState.inserting) this.ModelState = DataModelState.editing;
        }
    }

}
