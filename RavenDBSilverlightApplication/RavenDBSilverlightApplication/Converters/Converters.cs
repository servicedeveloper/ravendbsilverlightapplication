﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Linq;


namespace RavenDBSilverlightApplication
{

    public class ModelStateToBooleanConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is DataModelState)) return false;
            var param = parameter as string;
            if (param == null) return false;
            var invertor = param.IndexOf('!') == 0;
            if (invertor) param = param.Substring(1);
            var modes = param.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
            var result = modes.Any(mode => Enum.GetName(typeof(DataModelState), (DataModelState)value) == mode);
            if (invertor) result = !result;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }



}
