﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using Raven.Client;
using Raven.Client.Document;
using Raven.Client.Linq;
using System.Collections.Generic;
using System.Windows.Data;


namespace RavenDBSilverlightApplication
{

    public class RavenDBService : IDataModelService
    {

        //const string serverAddress = "http://servicedeveloper.org/Examples/RavenDBServer";
        //const string serverAddress = "http://localhost/RavenDBServer";
        const string serverAddress = "http://servicedeveloper-001-site1.smarterasp.net/Examples/RavenDBServer";

        DocumentStore documentStore = new DocumentStore { Url = serverAddress };

        public RavenDBService()
        {
            documentStore.Initialize();
        }

        public async void Get<T>(Action<IList<T>> callback) where T : CustomEntity
        {
            using (var session = documentStore.OpenAsyncSession("Adverts"))
            {
                var task = session.Query<T>().OrderBy(t => t.Id).ToListAsync();
                await task;
                if (callback != null) callback(task.Result);
            }
        }

        public async void Update<T>(T item, Action<Exception> callback) where T : CustomEntity
        {
            using (var session = documentStore.OpenAsyncSession("Adverts"))
            {
                await session.StoreAsync(item);
                var task = session.SaveChangesAsync();
                await task;
                if (callback != null) callback(task.Exception);
            }
        }

        public void Insert<T>(T item, Action<Exception> callback) where T : CustomEntity
        {
            this.Update(item, callback);
        }

        public async void Delete<T>(T item, Action<Exception> callback) where T : CustomEntity
        {
            using (var session = documentStore.OpenAsyncSession("Adverts"))
            {
                var entity = await session.LoadAsync<T>(item.Id);
                if (entity != null)
                {
                    session.Delete<T>(entity);
                    var task = session.SaveChangesAsync();
                    await task;
                    if (callback != null) callback(task.Exception);
                }
            }
        }


    }

}
