﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

using System.Collections.Generic;
using System.ComponentModel;


namespace RavenDBSilverlightApplication
{

    public abstract class CustomEntity : INotifyPropertyChanged
    {

        public long Id { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

    }



    public class Intention : CustomEntity
    {

        string name;
        public string Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                RaisePropertyChanged("Name");
            }
        }
    }



    public class Category : CustomEntity
    {

        string name;
        public string Name
        {
            get { return this.name; }
            set
            {
                this.name = value;
                RaisePropertyChanged("Name");
            }
        }

        public Category[] SubCategories { get; set; }

        public long[] IntentionIds { get; set; }


        public void AddSubCategory(Category category)
        {
            if (this.SubCategories == null)
                this.SubCategories = new Category[] { category };
            else
            {
                var subCategories = new List<Category>(this.SubCategories);
                subCategories.Add(category);
                this.SubCategories = subCategories.ToArray();
            }
        }

        public void ReplaceSubCategory(Category oldCategory, Category newCategory)
        {
            if (this.SubCategories == null)
                return;
            else
            {
                var subCategories = new List<Category>(this.SubCategories);
                this.SubCategories[subCategories.IndexOf(oldCategory)] = newCategory;
            }
        }

        public void DeleteSubCategory(Category category)
        {
            if (this.SubCategories == null)
                return;
            else
            {
                var subCategories = new List<Category>(this.SubCategories);
                subCategories.Remove(category);
                this.SubCategories = subCategories.Count == 0 ? null : subCategories.ToArray();
            }
        }

        public Category GetParent(Category root)
        {
            if (root.SubCategories != null)
                foreach (var subCategory in root.SubCategories)
                {
                    if (subCategory == this) return root;
                    var parent = GetParent(subCategory);
                    if (parent != null) return parent;
                }
            return null;
        }

    }


}
